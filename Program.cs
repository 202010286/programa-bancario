﻿using System;

namespace ProgramaBancario
{
    class ProgramaBancario
    {
        struct Clientes
        {
            public string nombre;
            public string apellido;
            public float saldo;
        }
        public static void Main()
        {
            Clientes cliente1;

            Console.Write("Ingrese el nombre del primer cliente: ");
            cliente1.nombre = Console.ReadLine();
            Console.Write("Ingrese el apellido del primer cliente: ");
            cliente1.apellido = Console.ReadLine();
            Console.Write("Ingrese el saldo del primer cliente: ");
            cliente1.saldo = float.Parse(Console.ReadLine());

            Clientes cliente2;

            Console.Write("\nIngrese el nombre del segundo cliente: ");
            cliente2.nombre = Console.ReadLine();
            Console.Write("Ingrese el apellido del segundo cliente: ");
            cliente2.apellido = Console.ReadLine();
            Console.Write("Ingrese el saldo del segundo cliente: ");
            cliente2.saldo = float.Parse(Console.ReadLine());

            Clientes cliente3;

            Console.Write("\nIngrese el nombre del tercer cliente: ");
            cliente3.nombre = Console.ReadLine();
            Console.Write("Ingrese el apellido del tercer cliente: ");
            cliente3.apellido = Console.ReadLine();
            Console.Write("Ingrese el saldo del tercer cliente: ");
            cliente3.saldo = float.Parse(Console.ReadLine());

            float saldo_total = cliente1.saldo + cliente2.saldo + cliente3.saldo;
            Console.Write($"El saldo total en las cuentas es: {saldo_total}");


        }
        
    }
}
